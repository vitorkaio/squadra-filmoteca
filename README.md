# squadra-filmoteca

**Projeto para o processo seletivo - Squadra. Api para o catálogo de filmes**

### Api

> https://polar-atoll-20679.herokuapp.com/

### Documentação

> https://documenter.getpostman.com/view/1339549/SzKN33ZH

### Tecnologias

* NodeJs
* Express
* Typescript
* Sequelize

### Observação

Api está hospedada na plataforma **[heroku](https://dashboard.heroku.com/)** e o banco de dados(postgres) está hospedado na nuvem utilizando a plataforma **[elephantsql](https://www.elephantsql.com/)**
