import { Model, DataTypes, Sequelize, HasManyAddAssociationMixin, HasManyRemoveAssociationMixin, ModelCtor } from 'sequelize'
import { ActorViewModel } from '../actors/actor.model'
import { DirectorViewModel } from '../directors/director.model'

export interface MovieAddModel {
    name: string;
}

export class MovieModel extends Model<MovieModel, MovieViewModel> {
  id: number;
  name: string;
  actors: ActorViewModel[];
  addActor: HasManyAddAssociationMixin<ActorViewModel, number>;
  removeActor: HasManyRemoveAssociationMixin<ActorViewModel, number>;
  addDirector: HasManyAddAssociationMixin<DirectorViewModel, number>;
  removeDirector: HasManyRemoveAssociationMixin<DirectorViewModel, number>;
}

export interface MovieViewModel {
    id: number;
    name: string;
    actors: ActorViewModel[];
}

export class Movie extends Model {
  static initStance (connection: Sequelize): void {
    super.init.call(this, {
      name: DataTypes.STRING
    }, {
      sequelize: connection
    })
  }

  static createAssociations (models: Record<string, ModelCtor<Model>>): void {
    this.belongsToMany(models.Actor, { foreignKey: 'movie_id', through: 'movie_actors', as: 'actors' })
    this.belongsToMany(models.Director, { foreignKey: 'movie_id', through: 'movie_directors', as: 'directors' })
  }
}// Fim da classe
