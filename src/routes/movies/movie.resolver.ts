import { Request, Response } from 'express'
import ResponseResolver from '../response/response'
import Codes from '../response/code'
import MovieController from './movie.controller'
import { MovieViewModel, MovieAddModel } from './movie.model'

class MovieResolver {
  // Retorna todos os filmes
  static async movies (req: Request, res: Response): Promise<Response> {
    try {
      const result = await MovieController.getMovies()
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Retorna um filme
  static async getMovie (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await MovieController.getMovie(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Cria um filme
  static async createMovie (req: Request, res: Response): Promise<Response> {
    try {
      const data: MovieViewModel = req.body
      const result = await MovieController.createMovie(data)
      return ResponseResolver.responseSuccess(res, Codes.CREATE, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Remove um filme
  static async removeMovie (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await MovieController.removeMovie(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Atualiza os dados do filme.
  static async updateMovie (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const data: MovieAddModel = req.body
      const result = await MovieController.updateMovie(parseInt(id), data)
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Adiciona um ator no filme
  static async addActorInMovie (req: Request, res: Response): Promise<Response> {
    try {
      const idMovie = req.params.idMovie
      const idActor = req.params.idActor
      const result = await MovieController.addActorInMovie(parseInt(idMovie), parseInt(idActor))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Remove um ator do filme
  static async removeActorInMovie (req: Request, res: Response): Promise<Response> {
    try {
      const idMovie = req.params.idMovie
      const idActor = req.params.idActor
      const result = await MovieController.removeActorInMovie(parseInt(idMovie), parseInt(idActor))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Adiciona um diretor no filme
  static async addDirectorInMovie (req: Request, res: Response): Promise<Response> {
    try {
      const idMovie = req.params.idMovie
      const idDirector = req.params.idDirector
      const result = await MovieController.addDirectorInMovie(parseInt(idMovie), parseInt(idDirector))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Remove um diretor do filme
  static async removeDirectorInMovie (req: Request, res: Response): Promise<Response> {
    try {
      const idMovie = req.params.idMovie
      const idDirector = req.params.idDirector
      const result = await MovieController.removeDirectorInMovie(parseInt(idMovie), parseInt(idDirector))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }
}// Fim da classe

export default MovieResolver
