import { Movie, MovieViewModel, MovieAddModel, MovieModel } from './movie.model'
import { ActorViewModel } from '../actors/actor.model'
import ActorController from '../actors/actor.controller'
import { DirectorViewModel } from '../directors/director.model'
import DirectorController from '../directors/director.controller'

class MovieController {
  static opsAttributes: Record<string, Record<string, string | string[] | Record<string, string[]>>[]> = {
    include: [{ association: 'actors', attributes: ['id', 'name'], through: { attributes: [] } },
      { association: 'directors', attributes: ['id', 'name'], through: { attributes: [] } }]
  }

  // Retorna todos os filmes
  static async getMovies (): Promise<MovieViewModel[]> {
    try {
      const movies: MovieViewModel[] = await Movie.findAll({
        ...this.opsAttributes,
        order: [
          ['id', 'ASC']
        ]
      })
      return movies
    } catch (error) {
      throw (error.message)
    }
  }

  // Retorna um filme
  static async getMovie (id: number): Promise<MovieViewModel> {
    try {
      // const movie: MovieViewModel = await Movie.findByPk(id, { attributes: { exclude: ['id'] } })
      const movie: MovieViewModel = await Movie.findByPk(id, { ...this.opsAttributes })
      return movie
    } catch (error) {
      throw (error.message)
    }
  }

  // Cria um filme
  static async createMovie (data: MovieAddModel): Promise<MovieViewModel> {
    try {
      const newMovie: MovieViewModel = await Movie.create(data)
      return newMovie
    } catch (error) {
      throw (error.message)
    }
  }

  // Remove um filme
  static async removeMovie (id: number): Promise<MovieModel> {
    try {
      const movie: MovieModel = await Movie.findByPk(id, { ...this.opsAttributes })
      if (!movie) {
        const message = { message: movie }
        throw (message)
      }
      await movie.destroy()
      return movie
    } catch (error) {
      throw (error.message)
    }
  }

  // Atualiza os dados do filme.
  static async updateMovie (id: number, data: MovieAddModel): Promise<MovieModel> {
    try {
      const movie: MovieModel = await Movie.findByPk(id)
      if (!movie) {
        const message = { message: movie }
        throw (message)
      }
      const [, newMovie] = await Movie.update({ ...data }, { where: { id: id }, returning: true })
      return newMovie
    } catch (error) {
      throw (error.message)
    }
  }

  // Adiciona um ator no filme
  static async addActorInMovie (idMovie: number, idActor: number): Promise<MovieViewModel> {
    try {
      const actor: ActorViewModel = await ActorController.getActor(idActor)
      if (!actor) {
        const message = { message: actor }
        throw (message)
      }
      const movie: MovieModel = await Movie.findByPk(idMovie)
      await movie.addActor(actor)
      return movie.reload({ ...this.opsAttributes })
    } catch (error) {
      throw (error.message)
    }
  }

  // Remove um ator do filme
  static async removeActorInMovie (idMovie: number, idActor: number): Promise<MovieViewModel> {
    try {
      const actor: ActorViewModel = await ActorController.getActor(idActor)
      if (!actor) {
        const message = { message: actor }
        throw (message)
      }
      const movie: MovieModel = await Movie.findByPk(idMovie)
      await movie.removeActor(actor)
      return movie.reload({ ...this.opsAttributes })
    } catch (error) {
      throw (error.message)
    }
  }

  // Adiciona um diretor no filme
  static async addDirectorInMovie (idMovie: number, idDirector: number): Promise<MovieViewModel> {
    try {
      const director: DirectorViewModel = await DirectorController.getDirector(idDirector)
      if (!director) {
        const message = { message: director }
        throw (message)
      }
      const movie: MovieModel = await Movie.findByPk(idMovie)
      await movie.addDirector(director)
      return movie.reload({ ...this.opsAttributes })
    } catch (error) {
      throw (error.message)
    }
  }

  // Remove um ator do filme
  static async removeDirectorInMovie (idMovie: number, idDirector: number): Promise<MovieViewModel> {
    try {
      const director: DirectorViewModel = await DirectorController.getDirector(idDirector)
      if (!director) {
        const message = { message: director }
        throw (message)
      }
      const movie: MovieModel = await Movie.findByPk(idMovie)
      await movie.removeDirector(director)
      return movie.reload({ ...this.opsAttributes })
    } catch (error) {
      throw (error.message)
    }
  }
}// Fim da classe

export default MovieController
