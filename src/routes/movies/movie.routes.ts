import express from 'express'
import MovieResolver from './movie.resolver'

const router = express.Router()

// GET /movies
router.get('/', MovieResolver.movies)

// GET /movies/id
router.get('/:id', MovieResolver.getMovie)

// POST /movies
router.post('/', MovieResolver.createMovie)

// Remove um filme
router.delete('/:id', MovieResolver.removeMovie)

// Atualiza os dados do filme.
router.patch('/:id', MovieResolver.updateMovie)

// Adiciona um ator no filme
router.patch('/:idMovie/actors/add/:idActor', MovieResolver.addActorInMovie)

// Remove um ator do filme
router.patch('/:idMovie/actors/remove/:idActor', MovieResolver.removeActorInMovie)

// Adiciona um diretor no filme
router.patch('/:idMovie/directors/add/:idDirector', MovieResolver.addDirectorInMovie)

// Remove um diretor do filme
router.patch('/:idMovie/directors/remove/:idDirector', MovieResolver.removeDirectorInMovie)

export default router
