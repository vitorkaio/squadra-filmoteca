import { Request, Response } from 'express'
import ResponseResolver from '../response/response'
import Codes from '../response/code'
import DirectorController from './director.controller'
import { DirectorViewModel } from './director.model'

class DirectorResolver {
  // Retorna todos os diretors
  static async directors (req: Request, res: Response): Promise<Response> {
    try {
      const result = await DirectorController.getDirectors()
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Retorna um diretor
  static async getDirector (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await DirectorController.getDirector(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Cria um diretor
  static async createDirector (req: Request, res: Response): Promise<Response> {
    try {
      const data: DirectorViewModel = req.body
      const result = await DirectorController.createDirector(data)
      return ResponseResolver.responseSuccess(res, Codes.CREATE, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Remove um diretor
  static async removeDirector (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await DirectorController.removeDirector(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }
}// Fim da classe

export default DirectorResolver
