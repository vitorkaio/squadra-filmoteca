import { Director, DirectorViewModel, DirectorAddModel, DirectorModel } from './director.model'

class DirectorController {
  // Retorna todos os diretores
  static async getDirectors (): Promise<DirectorViewModel[]> {
    try {
      const directors: DirectorViewModel[] = await Director.findAll({
        order: [
          ['id', 'ASC']
        ]
      })
      return directors
    } catch (error) {
      throw (error.message)
    }
  }

  // Retorna um diretor
  static async getDirector (id: number): Promise<DirectorViewModel> {
    try {
      const movie: DirectorViewModel = await Director.findByPk(id)
      return movie
    } catch (error) {
      throw (error.message)
    }
  }

  // Cria um diretor
  static async createDirector (data: DirectorAddModel): Promise<DirectorViewModel> {
    try {
      const newDirector: DirectorViewModel = await Director.create(data)
      return newDirector
    } catch (error) {
      throw (error.message)
    }
  }

  // Remove um diretor
  static async removeDirector (id: number): Promise<DirectorModel> {
    try {
      const director: DirectorModel = await Director.findByPk(id)
      if (!director) {
        const message = { message: director }
        throw (message)
      }
      director.destroy()
      return director
    } catch (error) {
      throw (error.message)
    }
  }
}// Fim da classe

export default DirectorController
