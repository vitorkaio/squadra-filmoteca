import express from 'express'
import DirectorResolver from './director.resolver'

const router = express.Router()

// GET /directors
router.get('/', DirectorResolver.directors)

// Retorna um ator
router.get('/:id', DirectorResolver.getDirector)

// Cria um novo ator
router.post('/', DirectorResolver.createDirector)

// Remove um ator
router.delete('/:id', DirectorResolver.removeDirector)

export default router
