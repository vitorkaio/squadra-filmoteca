import { Model, DataTypes, Sequelize, ModelCtor } from 'sequelize'

export interface DirectorAddModel {
  name: string;
}

export class DirectorModel extends Model<DirectorModel, DirectorViewModel> {
  id: number;
  name: string;
}

export interface DirectorViewModel {
  id: number;
  name: string;
}

export class Director extends Model {
  static initStance (sequelize: Sequelize): void {
    super.init.call(this, {
      name: DataTypes.STRING
    }, {
      sequelize
    })
  }

  static createAssociations (models: Record<string, ModelCtor<Model>>): void {
    this.belongsToMany(models.Movie, { foreignKey: 'director_id', through: 'movie_directors', as: 'movies' })
  }
}// Fim da classe
