import { Actor, ActorViewModel, ActorAddModel, ActorModel } from './actor.model'

class ActorController {
  // Retorna todos os filmes
  static async getActors (): Promise<ActorViewModel[]> {
    try {
      const actors: ActorViewModel[] = await Actor.findAll({
        order: [
          ['id', 'ASC']
        ]
      })
      return actors
    } catch (error) {
      throw (error.message)
    }
  }

  // Retorna um filme
  static async getActor (id: number): Promise<ActorViewModel> {
    try {
      const movie: ActorViewModel = await Actor.findByPk(id)
      return movie
    } catch (error) {
      throw (error.message)
    }
  }

  // Cria um filme
  static async createActor (data: ActorAddModel): Promise<ActorViewModel> {
    try {
      const newActor: ActorViewModel = await Actor.create(data)
      return newActor
    } catch (error) {
      throw (error.message)
    }
  }

  // Remove um ator
  static async removeActor (id: number): Promise<ActorModel> {
    try {
      const actor: ActorModel = await Actor.findByPk(id)
      if (!actor) {
        const message = { message: actor }
        throw (message)
      }
      actor.destroy()
      return actor
    } catch (error) {
      throw (error.message)
    }
  }
}// Fim da classe

export default ActorController
