import { Request, Response } from 'express'
import ResponseResolver from '../response/response'
import Codes from '../response/code'
import ActorController from './actor.controller'
import { ActorViewModel } from './actor.model'

class ActorResolver {
  // Retorna todos os filmes
  static async actors (req: Request, res: Response): Promise<Response> {
    try {
      const result = await ActorController.getActors()
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Retorna um filme
  static async getActor (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await ActorController.getActor(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Cria um filme
  static async createActor (req: Request, res: Response): Promise<Response> {
    try {
      const data: ActorViewModel = req.body
      const result = await ActorController.createActor(data)
      return ResponseResolver.responseSuccess(res, Codes.CREATE, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }

  // Remove um ator
  static async removeActor (req: Request, res: Response): Promise<Response> {
    try {
      const id = req.params.id
      const result = await ActorController.removeActor(parseInt(id))
      return ResponseResolver.responseSuccess(res, Codes.OK, result)
    } catch (error) {
      return ResponseResolver.ResponseErrors(res, error)
    }
  }
}// Fim da classe

export default ActorResolver
