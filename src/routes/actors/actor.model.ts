import { Model, DataTypes, Sequelize, ModelCtor } from 'sequelize'

export interface ActorAddModel {
  name: string;
}

export class ActorModel extends Model<ActorModel, ActorViewModel> {
  id: number;
  name: string;
}

export interface ActorViewModel {
  id: number;
  name: string;
}

export class Actor extends Model {
  static initStance (sequelize: Sequelize): void {
    super.init.call(this, {
      name: DataTypes.STRING
    }, {
      sequelize
    })
  }

  static createAssociations (models: Record<string, ModelCtor<Model>>): void {
    this.belongsToMany(models.Movie, { foreignKey: 'actor_id', through: 'movie_actors', as: 'movies' })
  }
}// Fim da classe
