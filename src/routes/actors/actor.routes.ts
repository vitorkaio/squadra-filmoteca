import express from 'express'
import ActorResolver from './actor.resolver'

const router = express.Router()

// GET /actors
router.get('/', ActorResolver.actors)

// Retorna um ator
router.get('/:id', ActorResolver.getActor)

// Cria um novo ator
router.post('/', ActorResolver.createActor)

// Remove um ator
router.delete('/:id', ActorResolver.removeActor)

export default router
