enum Code {
  OK = 200,
  CREATE = 201,
  NOT_FOUND = 404,
  CONFLICT = 409,
  ERROR = 400
}

export default Code
