import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import Database from './database'
import MovieRoutes from './routes/movies/movie.routes'
import ActorRoutes from './routes/actors/actor.routes'
import DirectorRoutes from './routes/directors/director.routes'

class Server {
  public express = express.application

  constructor () {
    this.express = express()

    this._middlewares()
    this._database()
    this._routes()
  }

  private _middlewares (): void {
    this.express.use(morgan('dev'))
    this.express.use(express.json())
    this.express.use(cors())
  }

  private _database (): void {
    Database.sequelize()
  }

  private _routes (): void {
    this.express.use('/movies', MovieRoutes)
    this.express.use('/actors', ActorRoutes)
    this.express.use('/directors', DirectorRoutes)
  }
}

export default new Server().express
