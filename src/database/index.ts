// import Sequelize from 'sequelize'
import { sequelize } from '../config/sequelize'

import { Movie } from '../routes/movies/movie.model'
import { Actor } from '../routes/actors/actor.model'
import { Director } from '../routes/directors/director.model'
import { Sequelize } from 'sequelize/types'

/* const connection = new Sequelize(dbConfig)

User.init(connection)
Address.init(connection)
Tech.init(connection)

User.associate(connection.models)
Address.associate(connection.models)
Tech.associate(connection.models) */

Movie.initStance(sequelize)
Actor.initStance(sequelize)
Director.initStance(sequelize)

Movie.createAssociations(sequelize.models)
Actor.createAssociations(sequelize.models)
Director.createAssociations(sequelize.models)

export default {
  sequelize: (): Sequelize => sequelize
}
